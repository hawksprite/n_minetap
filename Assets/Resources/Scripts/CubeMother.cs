﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CubeMother : MonoBehaviour {

    public GameObject cubePrefab;

    public float cubeDepth = 1;

    // Use this for initialization
    void Start()
    {

        SpawnLayer(1, Config.BlockType.Dirt, true);
        SpawnLayer(2, Config.BlockType.Dirt, true);
        SpawnLayer(3, Config.BlockType.Grass, false);
        //SpawnLayer(4, Color.cyan, true);
       // SpawnLayer(5, Color.magenta, false);

        int maxLayers = 5;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public GameObject neighbor;
    public void SpawnLayer(int radius, Config.BlockType type, bool randomSeed)
    {
        Vector3 root = transform.position;

        // Determine the spawn points for all the needed children
        List<Vector3> c = generatePointStructureRadius(radius);

        

        // Spawn them all
        for (int i = 0; i < c.Count; i++)
        {
            Vector3 loc = root + c[i];

            GameObject g = GameObject.Instantiate(cubePrefab, loc, Quaternion.identity) as GameObject;
            g.transform.parent = transform;

            if (randomSeed)
            {
                g.GetComponent<CubeHandler>().RandomlySeed();

                g.GetComponent<CubeHandler>().SetType(type);

                if (neighbor)
                {
                    g.GetComponent<CubeHandler>().neighbor = neighbor;
                }
            }

            neighbor = g;
        }
    }

    public List<Vector3> generatePointStructureRadius(float radius)
    {
        List<Vector3> l = new List<Vector3>();

        Vector3 f1 = new Vector3(cubeDepth * radius, cubeDepth * radius, -1 * cubeDepth * radius);
        Vector3 f2 = new Vector3(cubeDepth * radius, cubeDepth * radius, cubeDepth * radius);
        Vector3 f3 = new Vector3(cubeDepth * radius * -1, cubeDepth * radius, -1 * cubeDepth * radius);
        Vector3 f4 = new Vector3(cubeDepth * radius * -1, cubeDepth * radius, cubeDepth * radius);
        Vector3 f5 = new Vector3(cubeDepth * radius, cubeDepth * radius * -1, -1 * cubeDepth * radius);
        Vector3 f6 = new Vector3(cubeDepth * radius, cubeDepth * radius * -1, cubeDepth * radius);
        Vector3 f7 = new Vector3(cubeDepth * radius * -1, cubeDepth * radius * -1, -1 * cubeDepth * radius);
        Vector3 f8 = new Vector3(cubeDepth * radius * -1, cubeDepth * radius * -1, cubeDepth * radius);

        // Add the parent verts
        l.Add(f1);
        l.Add(f2);
        l.Add(f3);
        l.Add(f4);
        l.Add(f5);
        l.Add(f6);
        l.Add(f7);
        l.Add(f8);

        int depth = (int)Vector3.Distance(f1, f2) + 1;

        l = planeLocation(l, f7, depth, new Vector3(1, 0, 0), new Vector3(0, 1, 0));
        l = planeLocation(l, f7, depth, new Vector3(0, 0, 1), new Vector3(0, 1, 0));
        l = planeLocation(l, f5, depth, new Vector3(0, 0, 1), new Vector3(0, 1, 0));
        l = planeLocation(l, f8, depth, new Vector3(1, 0, 0), new Vector3(0, 1, 0));
        l = planeLocation(l, f3, depth, new Vector3(1, 0, 0), new Vector3(0, 0, 1));
        l = planeLocation(l, f7, depth, new Vector3(1, 0, 0), new Vector3(0, 0, 1));

        return l;
    }

    public List<Vector3> planeLocation(List<Vector3> o, Vector3 c1, int depth, Vector3 axisX, Vector3 axisY)
    {
        List<Vector3> l = new List<Vector3>();

        foreach (Vector3 oi in o)
        {
            l.Add(oi);
        }

        for (int x = 0; x < depth; x++)
        {
            for (int y = 0; y < depth; y++)
            {
                Vector3 root = new Vector3();

                root.x += axisX.x * x;
                root.x += axisY.x * y;

                root.y += axisX.y * x;
                root.y += axisY.y * y;

                root.z += axisX.z * x;
                root.z += axisY.z * y;

                Vector3 newV = root + c1;

                if (l.Contains(newV) == false)
                    l.Add(newV);
            }
        }


        return l;
    }

    void OnMouseDown()
    {
        Application.LoadLevel(0);
    }
}
