﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
    [HideInInspector]
    public float targetYR = 0;
    [HideInInspector]
    public float targetXR = 0;
    [HideInInspector]
    public float currentXR = 0;
    [HideInInspector]
    public float currentYR = 0;

    public float rotationSpeed = 10.0f;

    public GameObject sphere;

    public float cameraDistance = 30.0f;

    public GameObject tool1;

	public bool rotateObject = true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetMouseButtonDown(1))
        {
            Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(r, out hit, 1000.0f))
            {
                GameObject g = (GameObject)GameObject.Instantiate(tool1, hit.transform.position, Quaternion.identity);
                g.transform.parent = sphere.transform;
            }
        }

        updateControlls();
	}

    void updateControlls()
    {
        if (Input.GetKey(KeyCode.W))
        {
            targetXR += 1.0f;
        }

        if (Input.GetKey(KeyCode.A))
        {
            targetYR += 1.0f;
        }

        if (Input.GetKey(KeyCode.S))
        {
            targetXR -= 1.0f;
        }

        if (Input.GetKey(KeyCode.D))
        {
            targetYR -= 1.0f;
        }

        cameraDistance += Input.GetAxis("Mouse ScrollWheel") * 120.0f * Time.smoothDeltaTime;

        currentXR = Mathf.Lerp(currentXR, targetXR, Time.smoothDeltaTime * rotationSpeed);
        currentYR = Mathf.Lerp(currentYR, targetYR, Time.smoothDeltaTime * rotationSpeed);

		if (rotateObject) {
			sphere.transform.rotation = Quaternion.Euler(currentXR, currentYR, 0);
		} else {
			Quaternion q = Quaternion.Euler (currentXR, currentYR, 0);
			Vector3 direction = q * Vector3.forward;
			transform.position = sphere.transform.position - direction * cameraDistance;
		}

        transform.LookAt(sphere.transform);

		if (rotateObject) {
			transform.position = new Vector3 (cameraDistance, 0, 0);
		}
    }
}
