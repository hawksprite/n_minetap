﻿using UnityEngine;
using System.Collections;

public class CubeHandler : MonoBehaviour {

    public GameObject breakPrefab;

    public float Health = 3;

    public bool useGravity = false;

    Rigidbody r;

    public GameObject star2;

    public GameObject neighbor;

    public Config.BlockType type;

	// Use this for initialization
	void Start () {
        r = GetComponent<Rigidbody>();
        star2 = GameObject.FindGameObjectWithTag("Gravity") as GameObject;
	}

	// Update is called once per frame
	void Update () {
        if (Health <= 0)
        {
            Death();
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            r.AddForce(new Vector3(Random.Range(-50, 50), Random.Range(-50, 50), Random.Range(-50, 50)));
        }


        if (useGravity) { 
            r.AddForce((transform.parent.position - transform.position) * 50.0f * Time.smoothDeltaTime);
           // r.AddForce((star2.transform.position - transform.position) * 20.0f * Time.smoothDeltaTime);

            if (neighbor)
            {
                r.AddForce((neighbor.transform.position - transform.position) * 20.0f * Time.smoothDeltaTime);
            }
        }
	}

    public void SetType(Config.BlockType newType)
    {
        type = newType;

        switch (type)
        {
            case Config.BlockType.Grass:
			GetComponent<MeshRenderer>().material.color = Color.green;

                break;
            case Config.BlockType.Dirt:
			GetComponent<MeshRenderer>().material.color = Color.black;
                break;

            case Config.BlockType.Sand:
			GetComponent<MeshRenderer>().material.color = Color.magenta;
                break;
        }
    }

    public void Death()
    {
        if (breakPrefab)
        {
            GameObject.Instantiate(breakPrefab, transform.position, transform.rotation);
        }

        Destroy(this.gameObject);
    }

    public void Hit()
    {
        Health -= 1;
    }

    public void RandomlySeed()
    {
        float chance = Random.Range(0, 100);

        if (chance <= 26)
        {
            //SetType(Config.BlockType.Sand);
        }
    }

    void OnMouseDown()
    {
        Hit();
    }

    void OnTriggerEnter(Collider other) {
        Debug.Log("HIT");

        Hit();

        if (other.tag == "Weapon")
        {
            
        }
    }
}
